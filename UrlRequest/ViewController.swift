//
//  ViewController.swift
//  UrlRequest
//
//  Created by Leandro Ramos on 2/29/20.
//  Copyright © 2020 Black Beard Games. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UINavigationController, NetworkingDelegate {
    
    var network = Networking()// also used in delegate
                
    lazy var customSearchBar: UISearchBar = {
        UISearchBar(frame: .zero)
    }()
    
    lazy var titleLabel: UILabel = {
        UILabel()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        manageBackground()
        setUpLayout()
        constraints()
        componetsSetUp()
        configureNavigation()
        network.delegate = self// set the delegate before the request
        network.makeRequest()
    }
}

extension ViewController: ViewLayout {
    
    func manageBackground() {
        view.backgroundColor = .white
    }
    
    func setUpLayout() {
        view.addSubview(customSearchBar)
        view.addSubview(titleLabel)
    }
    
    func constraints() {
                                
        customSearchBar.snp.makeConstraints { (maker) in
            
        }
        
        titleLabel.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
            maker.width.equalToSuperview().inset(16)
        }
    }
    
    func componetsSetUp() {
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        titleLabel.layer.borderColor = UIColor.black.cgColor
        titleLabel.layer.borderWidth = 0.5
        titleLabel.layer.cornerRadius = 10
        titleLabel.clipsToBounds = true
    }
    
    func didUpdateMovie(_ network: Networking, movie: Movie) {
        DispatchQueue.main.async {
            self.titleLabel.text = movie.Writer
        }
    }
    
    func didFailWithError(error: Error) {
        print(error)
    }

    func configureNavigation() {
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Search Movies"
    }
}

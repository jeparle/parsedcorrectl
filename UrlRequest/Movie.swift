//
//  Movie.swift
//  UrlRequest
//
//  Created by Leandro Ramos on 2/29/20.
//  Copyright © 2020 Black Beard Games. All rights reserved.
//

import Foundation

struct Movie: Decodable {
    
    var Title: String
    let Year: String
    let Rated: String
    let Writer: String
    let Released: String
    let Runtime: String
    let Genre: String
    let Director: String
    let Actors: String
    let Plot: String
    let Language: String
    let Country: String
    let Awards: String
    let Poster: URL
    let Ratings: [Ratings]
    let Metascore: String
    let imdbRating: String
    let imdbVotes: String
    let imdbID: String
    let `Type`: String
    let DVD: String
    let BoxOffice: String
    let Production: String
    }

    struct Ratings: Codable {
        let Source: String
        let Value : String
}


//
//  Protocol.swift
//  UrlRequest
//
//  Created by Leandro Ramos on 2/29/20.
//  Copyright © 2020 Black Beard Games. All rights reserved.
//

import Foundation

protocol ViewLayout {
    func manageBackground()
    func setUpLayout()
    func constraints()
    func componetsSetUp()
}

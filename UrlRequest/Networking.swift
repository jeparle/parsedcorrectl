//
//  Networking.swift
//  UrlRequest
//
//  Created by Leandro Ramos on 2/29/20.
//  Copyright © 2020 Black Beard Games. All rights reserved.
//

import Foundation

protocol NetworkingDelegate {//protocol in the same file of the class whos delegate
    func didUpdateMovie(_ network: Networking, movie: Movie)//the model of json as parameters
    func didFailWithError(error: Error)
}

struct Networking {
    
    let urlJson = "https://www.omdbapi.com/?i=tt3896198&apikey=b6531970"
    
    var delegate: NetworkingDelegate?// delegate variable, also in the class whos delegate

    func makeRequest() {
        
        guard let url = URL(string: urlJson) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                self.delegate?.didFailWithError(error: error!)
                return
            }
            
            guard let data = data else { return }
                        
            do {
                let movie = try JSONDecoder().decode(Movie.self, from: data)
                self.delegate?.didUpdateMovie(self, movie: movie)
            } catch {
                self.delegate?.didFailWithError(error: error)
                print("failed to encode the data: \(error.localizedDescription)")
                
            }
        }.resume()
    }
}
